document.addEventListener('DOMContentLoaded', () => {
    const start = document.getElementById('start');
    start.style.opacity = '0';
    setTimeout(() => start.style.display = 'none', 3500);
});
