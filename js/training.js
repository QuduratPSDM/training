window.onload = function() {
    sessionStorage.clear();
}

const [chapters, choices, back] = ['chapters', 'choices', 'back'].map(id => document.getElementById(id));
const [withTime, noTime] = ['withTime', 'noTime'].map(id => document.getElementById(id));

Array.from(chapters.children).forEach(child => 
    child.addEventListener('click', () => {
        chapters.style.display = 'none';
        choices.style.display = 'flex';

        let numericId = child.id.replace('item', '');

        withTime.href = `chapter${numericId}.html`;
        noTime.href = `chapter${numericId}.html?notime`;
    })
);


back.addEventListener('click', () => {
    chapters.style.display = 'flex';
    choices.style.display = 'none';

    withTime.href = "";
    noTime.href = "";
});
