const countdownWrapper = document.getElementById('countdownWrapper')
const overlay = document.getElementById('overlay')
const countdown = document.getElementById('countdown');
const form = document.getElementById('form');
const timeEnd = document.getElementById('timeEnd');
const reveal = document.getElementById('reveal');
const habis = document.getElementById('habis');


const urlParams = new URLSearchParams(window.location.search);

if (!urlParams.has('notime')) {
    window.addEventListener('DOMContentLoaded', () => {
        let endTime;
        const storedEndTime = sessionStorage.getItem('endTime');

        if (storedEndTime) {
            endTime = new Date(storedEndTime);
        } else {
            endTime = new Date(Date.now() + 20 * 60 * 1000);
            sessionStorage.setItem('endTime', endTime);
        }

        const updateCountdown = () => {
            const diff = endTime - Date.now();

            const minutes = Math.floor((diff % (1000 * 60 * 60)) / (1000 * 60));
            const seconds = Math.floor((diff % (1000 * 60)) / 1000);

            countdown.textContent = `${String(minutes).padStart(2, '0')}:${String(seconds).padStart(2, '0')}`;

            if (diff <= 30000 && diff > 0) {
                overlay.style.display = 'flex'
                countdownWrapper.style.cssText = 'font-size: 2em; color: #FB3640; background-color: transparent';
                setTimeout(function () {
                    overlay.style.opacity = '1';
                }, 100);
            } else {
                overlay.style.opacity = '0';
            };

            if (diff > 0) {
                form.style.display = 'flex';
                setTimeout(updateCountdown, 1000);
            } else {
                countdown.textContent = '00:00';
                form.style.display = 'none';
                habis.style.display = 'flex';
                sessionStorage.removeItem('endTime');
            }            
        };
        updateCountdown();
    });
}

reveal.addEventListener('click', () => {
    form.style.display = 'flex';
    habis.style.display = 'none';
});
